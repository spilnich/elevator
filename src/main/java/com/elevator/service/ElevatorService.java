package com.elevator.service;

import com.elevator.enums.ButtonType;
import com.elevator.model.Elevator;
import com.elevator.model.Floor;
import com.elevator.model.Passenger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.elevator.model.Building.FLOORS_AMOUNT;

public class ElevatorService {

    private final Elevator elevator;

    public ElevatorService(Elevator elevator) {
        this.elevator = elevator;
    }

    public void driveByRoute(List<Floor> floors) {
        initElevatorPassengers(floors.get(elevator.getCurrentPosition() - 1));
        initEdgeFloors();
        checkElevatorStartPosition(floors);
        runElevator(floors);
        driveByRoute(floors);
    }

    private void runElevator(List<Floor> floors) {
        if (elevator.getCurrentPosition() == elevator.getTempMaxFloor()) {
            elevator.setButtonType(ButtonType.DOWN);
            for (int floorNumber = elevator.getCurrentPosition(); floorNumber >= elevator.getMinFloor(); floorNumber--) {
                elevatorRunProcess(floors, floorNumber);
            }
        } else {
            elevator.setButtonType(ButtonType.UP);
            initEdgeFloors();
            for (int floorNumber = elevator.getCurrentPosition(); floorNumber <= elevator.getMaxFloor(); floorNumber++) {
                elevator.setTempMaxFloor(floorNumber);
                elevatorRunProcess(floors, floorNumber);
            }
        }
        getNewDirection(floors);
    }

    public void getNewDirection(List<Floor> floors) {
        if (elevator.getPassengers().isEmpty() && floors.get(elevator.getCurrentPosition() - 1).getPassengers().isEmpty()) {
            setNearestFloorOnDemand(floors);
        }
    }

    private void setNearestFloorOnDemand(List<Floor> floors) {
        if (elevator.getCurrentPosition() != 1) {
            for (int floorNumber = 1; floorNumber < elevator.getCurrentPosition(); floorNumber++) {
                setCurrentPositionInNoPassengersSituation(floors, floorNumber);
            }
        } else {
            for (int floorNumber = elevator.getCurrentPosition(); floorNumber < FLOORS_AMOUNT; floorNumber++) {
                setCurrentPositionInNoPassengersSituation(floors, floorNumber);
                if (isCurrentFloorEmpty(floors, floorNumber)) {
                    break;
                }
            }
        }
    }

    private void setCurrentPositionInNoPassengersSituation(List<Floor> floors, int floorNumber) {
        if (isCurrentFloorEmpty(floors, floorNumber)) {
            elevator.setCurrentPosition(floorNumber);
        }
    }

    private boolean isCurrentFloorEmpty(List<Floor> floors, int floorNumber) {
        return !floors.get(floorNumber - 1).getPassengers().isEmpty();
    }

    private void sleep() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            System.out.println("Interrupted exception occurred!");
            Thread.currentThread().interrupt();
        }
    }

    private void elevatorRunProcess(List<Floor> floors, int floorNumber) {
        elevator.setCurrentPosition(floorNumber);
        leaveElevator(floors.get(floorNumber - 1));
        enterElevator(floors.get(floorNumber - 1));
        printFloorBanner(floors, floorNumber);
        printElevatorStage(floors, elevator.getPassengers());
        sleep();
    }

    private void printFloorBanner(List<Floor> floors, int floorNumber) {
        System.out.println();
        System.out.println("***FLOOR " + floors.get(floorNumber - 1).getFloorNumber() + "***");
        System.out.println();
    }

    private void checkElevatorStartPosition(List<Floor> floors) {
        if (elevator.isOnStartPosition()) {
            elevator.setMinFloor(elevator.getCurrentPosition());
            runElevator(floors);
            elevator.setOnStartPosition(false);
        }
    }

    private int getMaxFloor(List<Passenger> passengers) {
        return Collections.max(getFloorRout(passengers));
    }

    private int getMinFloor(List<Passenger> passengers) {
        return Collections.min(getFloorRout(passengers));
    }

    private List<Integer> getFloorRout(List<Passenger> passengers) {
        return passengers.stream()
                .map(Passenger::getRequiredFloor)
                .map(Floor::getFloorNumber)
                .collect(Collectors.toList());
    }

    public void enterElevator(Floor floor) {
        new ArrayList<>(floor.getPassengers()).stream()
                .filter(passenger -> (elevator.getButtonType() == ButtonType.UP &&
                        passenger.getRequiredFloor().getFloorNumber() > elevator.getCurrentPosition() && getFreePlaces() > 0) ||
                        (elevator.getButtonType() == ButtonType.DOWN &&
                                passenger.getRequiredFloor().getFloorNumber() < elevator.getCurrentPosition() && getFreePlaces() > 0))
                .limit(getFreePlaces())
                .forEach(passenger -> {
                    movePassengers(floor, passenger);
                    initEdgeFloors();
                });
    }

    public void leaveElevator(Floor floor) {
        new ArrayList<>(elevator.getPassengers()).stream()
                .filter(passenger -> passenger.getRequiredFloor().getFloorNumber() == elevator.getCurrentPosition())
                .forEach(passenger -> {
                    elevator.getPassengers().remove(passenger);
                    floor.getPassengers().add(passenger);
                    passenger.setRequiredFloor(Floor.generateRequiredFloor(FLOORS_AMOUNT));
                });
    }

    public int getFreePlaces() {
        int elevatorLoad = elevator.getPassengers().size();
        return elevatorLoad < Elevator.CAPACITY ? Elevator.CAPACITY - elevatorLoad : 0;
    }

    private void printElevatorStage(List<Floor> floors, List<Passenger> elevatorPassengers) {
        floors.forEach(floor -> {
            if (floor.getFloorNumber() == elevator.getCurrentPosition()) {
                System.out.println("|#" + elevatorPassengers + "#        |" + floor.getPassengers());
            } else {
                System.out.println("|                           |" + floor.getPassengers());
            }
        });
    }

    public void initElevatorPassengers(Floor floor) {
        new ArrayList<>(floor.getPassengers()).stream()
                .limit(5)
                .forEach(passenger -> movePassengers(floor, passenger));
    }

    private void movePassengers(Floor floor, Passenger passenger) {
        elevator.getPassengers().add(passenger);
        floor.getPassengers().remove(passenger);
    }

    public void initEdgeFloors() {
        elevator.setMinFloor(getMinFloor(elevator.getPassengers()));
        elevator.setMaxFloor(getMaxFloor(elevator.getPassengers()));
    }
}