package com.elevator.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class Building {

    public static final int FLOORS_AMOUNT;
    private final List<Floor> floors;

    static {
        FLOORS_AMOUNT = getTotalFloorsAmount();
    }

    public Building() {
        floors = new ArrayList<>();
        fillFloorsWithPassengers();
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public int getFloorsAmount() {
        return FLOORS_AMOUNT;
    }

    public static int getTotalFloorsAmount() {
        return ThreadLocalRandom.current().nextInt(5, 21);
    }

    private void fillFloorsWithPassengers() {
        IntStream.rangeClosed(1, FLOORS_AMOUNT)
                .forEach(number -> floors.add(new Floor(number)));
    }
}
