package com.elevator.model;


public class Passenger {

    private Floor requiredFloor;

    public Passenger(Floor requiredFloor) {
        this.requiredFloor = requiredFloor;
    }

    public Floor getRequiredFloor() {
        return requiredFloor;
    }

    public void setRequiredFloor(Floor requiredFloor) {
        this.requiredFloor = requiredFloor;
    }

    @Override
    public String toString() {
        return String.valueOf(requiredFloor.getFloorNumber());
    }
}
