package com.elevator;

import com.elevator.model.Building;
import com.elevator.model.Elevator;
import com.elevator.service.ElevatorService;

public class Solution {

    public static void main(String[] args) {
        Building building = new Building();
        building.getFloors().forEach(floor -> floor.generatePassengers(building.getFloorsAmount()));
        Elevator elevator = new Elevator();
        ElevatorService elevatorService = new ElevatorService(elevator);
        elevatorService.driveByRoute(building.getFloors());
    }
}
