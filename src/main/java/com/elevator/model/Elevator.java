package com.elevator.model;


import com.elevator.enums.ButtonType;

import java.util.ArrayList;
import java.util.List;

public class Elevator {

    public static final int CAPACITY = 5;
    private int currentPosition;
    private int minFloor;
    private int maxFloor;
    private int tempMaxFloor;
    private boolean isOnStartPosition;
    private List<Passenger> passengers;
    private ButtonType buttonType;

    public Elevator() {
        passengers = new ArrayList<>();
        isOnStartPosition = true;
        currentPosition = 1;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public int getMinFloor() {
        return minFloor;
    }

    public void setMinFloor(int minFloor) {
        this.minFloor = minFloor;
    }

    public int getMaxFloor() {
        return maxFloor;
    }

    public void setMaxFloor(int maxFloor) {
        this.maxFloor = maxFloor;
    }

    public int getTempMaxFloor() {
        return tempMaxFloor;
    }

    public void setTempMaxFloor(int tempMaxFloor) {
        this.tempMaxFloor = tempMaxFloor;
    }

    public boolean isOnStartPosition() {
        return isOnStartPosition;
    }

    public void setOnStartPosition(boolean onStartPosition) {
        isOnStartPosition = onStartPosition;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public ButtonType getButtonType() {
        return buttonType;
    }

    public void setButtonType(ButtonType buttonType) {
        this.buttonType = buttonType;
    }
}