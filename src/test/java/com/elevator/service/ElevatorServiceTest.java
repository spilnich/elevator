package com.elevator.service;

import com.elevator.enums.ButtonType;
import com.elevator.model.Building;
import com.elevator.model.Elevator;
import com.elevator.model.Floor;
import com.elevator.model.Passenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ElevatorServiceTest {
    private Building building;
    private Elevator elevator;
    private ElevatorService elevatorService;
    private Floor floor1;
    private Floor floor2;
    private Floor floor3;
    private Passenger passenger1;
    private Passenger passenger2;
    private Passenger passenger3;
    private Passenger passenger4;
    private Passenger passenger5;
    private Passenger passenger6;


    @BeforeEach
    void setUp() {
        building = new Building();
        elevator = new Elevator();
        elevatorService = new ElevatorService(elevator);

        createFloors();
        createPassengers();
        addFloorPassengers();
        addBuildingFloors();
    }

    private void addBuildingFloors() {
        building.getFloors().clear();
        building.getFloors().add(floor1);
        building.getFloors().add(floor2);
        building.getFloors().add(floor3);
    }

    private void addFloorPassengers() {
        floor1.getPassengers().add(passenger2);
        floor1.getPassengers().add(passenger5);
        floor2.getPassengers().add(passenger3);
        floor2.getPassengers().add(passenger4);
        floor3.getPassengers().add(passenger1);
    }

    private void createPassengers() {
        passenger1 = new Passenger(floor1);
        passenger2 = new Passenger(floor2);
        passenger3 = new Passenger(floor3);
        passenger4 = new Passenger(floor3);
        passenger5 = new Passenger(floor3);
        passenger6 = new Passenger(floor3);
    }

    private void createFloors() {
        floor1 = new Floor(1);
        floor2 = new Floor(2);
        floor3 = new Floor(3);
    }

    @Test
    @DisplayName("Elevator should moves DOWN onto 1 floor in case current is empty and 1 floor with passengers")
    void getElevatorNewLowerPosition() {
        floor2.getPassengers().clear();
        elevator.setPassengers(Collections.emptyList());
        elevator.setCurrentPosition(2);
        elevatorService.getNewDirection(List.of(floor1, floor2, floor3));
        assertThat(elevator.getCurrentPosition()).isEqualTo(1);
    }

    @Test
    @DisplayName("Elevator should moves UP onto 3 floor in case previous floors are empty")
    void getElevatorNewHigherPosition() {
        floor1.getPassengers().clear();
        floor2.getPassengers().clear();
        elevator.setPassengers(Collections.emptyList());
        elevator.setCurrentPosition(1);
        System.out.println(building.getFloorsAmount());
        var floors = new ArrayList<>(Arrays.asList(floor1, floor2));
        IntStream.rangeClosed(2, building.getFloorsAmount()).forEach(number -> floors.add(floor3));
        elevatorService.getNewDirection(floors);
        assertThat(elevator.getCurrentPosition()).isEqualTo(3);
    }

    @Test
    @DisplayName("Two passengers entered empty elevator in UP direction")
    void enterEmptyElevatorDirectionUp() {
        elevator.setCurrentPosition(2);
        elevator.setButtonType(ButtonType.UP);
        elevatorService.enterElevator(floor2);
        assertTrue(elevator.getPassengers().containsAll(List.of(passenger3, passenger4)));
    }

    @Test
    @DisplayName("One passenger entered empty elevator in DOWN direction")
    void enterEmptyElevatorDirectionDown() {
        elevator.setCurrentPosition(3);
        elevator.setButtonType(ButtonType.DOWN);
        elevatorService.enterElevator(floor3);
        assertTrue(elevator.getPassengers().contains(passenger1));
    }

    @Test
    @DisplayName("One passenger cant enter full elevator")
    void enterFullElevator() {
        elevator.setPassengers(List.of(passenger1, passenger2, passenger3, passenger4, passenger5));
        elevatorService.enterElevator(floor3);
        assertFalse(elevator.getPassengers().contains(passenger6));
    }

    @Test
    @DisplayName("Passenger 2 left elevator on required floor")
    void leaveElevator() {
        elevator.setPassengers(new ArrayList<>(Arrays.asList(passenger1, passenger2)));
        elevator.setCurrentPosition(2);
        elevatorService.leaveElevator(floor2);
        assertFalse(elevator.getPassengers().contains(passenger2));
    }

    @Test
    @DisplayName("Should return 5 in case of no passengers in elevator")
    void getFreePlacesInEmptyElevator() {
        elevator.setPassengers(Collections.emptyList());
        assertThat(elevatorService.getFreePlaces()).isEqualTo(5);
    }

    @Test
    @DisplayName("Should return 0 in case of no passengers in elevator")
    void getFreePlacesInFullElevator() {
        elevator.setPassengers(List.of(passenger1, passenger2, passenger3, passenger4, passenger5));
        assertThat(elevatorService.getFreePlaces()).isZero();
    }

    @Test
    @DisplayName("Should return 0 in case of all passengers went from floor to elevator")
    void allPassengersWentFromFloor() {
        elevatorService.initElevatorPassengers(floor1);
        assertThat(floor1.getPassengers().size()).isZero();
    }

    @Test
    @DisplayName("Should return 2 in case of two passengers entered elevator")
    void initElevatorPassengers() {
        elevatorService.initElevatorPassengers(floor1);
        assertThat(elevator.getPassengers().size()).isEqualTo(2);
    }

    @Test
    @DisplayName("Should return min destination floor 1, max destination floor 3")
    void initEdgeFloors() {
        elevator.getPassengers().addAll(List.of(passenger1, passenger2, passenger3, passenger4, passenger5));
        elevatorService.initEdgeFloors();
        assertThat(elevator.getMinFloor()).isEqualTo(1);
        assertThat(elevator.getMaxFloor()).isEqualTo(3);
    }
}