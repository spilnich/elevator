package com.elevator.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class Floor {

    private final int floorNumber;
    private final List<Passenger> passengers;

    public Floor(int floorNumber) {
        passengers = new ArrayList<>();
        this.floorNumber = floorNumber;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    private int getPassengersAmount() {
        return ThreadLocalRandom.current().nextInt(0, 11);
    }

    public void generatePassengers(int maxFloor) {
        IntStream.rangeClosed(0, getPassengersAmount())
                .forEach(passenger -> passengers.add(new Passenger(generateRequiredFloor(maxFloor))));
    }

    public static Floor generateRequiredFloor(int maxFloor) {
        return new Floor(ThreadLocalRandom.current().nextInt(1, maxFloor + 1));
    }
}
